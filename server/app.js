// TODO: Add ability to choose manager when registering a department. An existing manager shouldn't be
// TODO: allowed on the list. Saving of department in departments table and of manager in dept_managers table should be
// TODO: one  transaction (i.e., atomic)

// TODO: 4. Add route that would handle retrieval of employees that are not managers and would return first name, last
// TODO: name, and emp no to client
// TODO: 6. Update POST api/departments route to include insertion of new department and its manager into the
// TODO: 6. dept_manager table

// DEPENDENCIES ------------------------------------------------------------------------------------------------------
// Loads express module and assigns it to a var called express
var express = require("express");

// Loads path to access helper functions for working with files and directory paths
var path = require("path");

// Loads bodyParser to populate and parse the body property of the request object
var bodyParser = require("body-parser");

// Loads sequelize ORM
var Sequelize = require("sequelize");

// CONSTANTS ---------------------------------------------------------------------------------------------------------
// Defines server port.
// Value of NODE_PORT is taken from the user environment if defined; port 3000 is used otherwise.
const NODE_PORT = process.env.NODE_PORT || 3000;

// Defines paths
// __dirname is a global that holds the directory name of the current module
const CLIENT_FOLDER = path.join(__dirname, '/../client');  // CLIENT FOLDER is the public directory
const MSG_FOLDER = path.join(CLIENT_FOLDER, '/assets/messages');

// Defines MySQL configuration
const MYSQL_USERNAME = 'root';
const MYSQL_PASSWORD = 'loveb@dminton88';

// OTHER VARS ---------------------------------------------------------------------------------------------------------
// Creates an instance of express called app
var app = express();

// DBs, MODELS, and ASSOCIATIONS ---------------------------------------------------------------------------------------
// Creates a MySQL connection
var sequelize = new Sequelize(
    'shop',
    MYSQL_USERNAME,
    MYSQL_PASSWORD,
    {
        host: 'localhost',         // default port    : 3306
        logging: console.log,
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    }
);

// Loads model for product table
var Product = require('./models/product')(sequelize, Sequelize);

// Associations. Reference: https://dev.mysql.com/doc/employee/en/sakila-structure.html
// Link Department model to DeptManager model through the dept_no FK. This relationship is 1-to-N and so we use hasMany
// Link DeptManager model to Employee model through the emp_no FK. This relationship is N-to-1 and so we use hasOne
//Department.hasMany(Manager, {foreignKey: 'dept_no'});
//Manager.hasOne(Employee, {foreignKey: 'emp_no'});

// MIDDLEWARES --------------------------------------------------------------------------------------------------------

// Serves files from public directory (in this case CLIENT_FOLDER).
// __dirname is the absolute path of the application directory.
// if you have not defined a handler for "/" before this line, server will look for index.html in CLIENT_FOLDER
app.use(express.static(CLIENT_FOLDER));

// Populates req.body with information submitted through the registration form.
// Default $http content type is application/json so we use json as the parser type
// For content type is application/x-www-form-urlencoded  use: app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());


// ROUTE HANDLERS -----------------------------------------------------------------------------------------------------
// Defines endpoint handler exposed to client side for retrieving department records that match query string passed.
// Match against dept name and dept no. Includes manager information. Client side sent data as part of the query
// string, we access query string paramters via the req.query property
app.get("/api/product", function (req, res) {
    Product
        // Use findAll to retrieve multiple records
        .findAll({
            // Use the where clause to filter final result; e.g., when you only want to retrieve departments that have
            // "s" in its name
            where: {
                // $or operator tells sequelize to retrieve record that match any of the condition
                $or: [
                    // $like + % tells sequelize that matching is not a strict matching, but a pattern match
                    // % allows you to match any string of zero or more characters
                    { brand: { $like: "%" + req.query.searchString + "%" } },
                    { name: { $like: "%" + req.query.searchString + "%" } }
                ]
            }
           , order: [["name", "ASC"]]
                , limit: 20 
            // What Include attribute does: Join two or more tables. In this instance:
            // 1. For every Department record that matches the where condition, the include attribute returns
            // ALL employees that have served as managers of said Department
            // 2. model attribute specifies which model to join with primary model
            // 3. order attribute specifies that the list of Managers be ordered from latest to earliest manager
            // 4. limit attribute specifies that only 1 record (in this case the latest manager) should be returned

        })
        // this .then() handles successful findAll operation
        // in this example, findAll() used the callback function to return departments
        // we named it departments, but this object also contains info about the
        // latest manager of that department
        .then(function (product) {
            res
                .status(200)
                .json(product);
        })
        // this .catch() handles erroneous findAll operation
        .catch(function (err) {
            res
                .status(500)
                .json(err);
        });
});


// -- Retrieve product info
app.get('/api/product/:id', function (req, res) {
    Product
        .findOne(
        { brand: req.body.brand },
        { name: req.body.name },
        { upc12: req.body.upc12 }
        , { where: { id: req.params.id } }
        )
        .then(function (product) {
            res
                .status(200)
                .json(product);
        })
        .catch(function (err) {
            res
                .status(500)
                .json(err);
        });
});




// -- Updates product info
app.put('/api/product/:id', function (req, res) {
    Product
        .update(
        { brand: req.body.brand },
        { name: req.body.name },
        { upc12: req.body.upc12 }
        , { where: { id: req.params.id } }
        )
        .then(function (product) {
            res
                .status(200)
                .json(product);
        })
        .catch(function (err) {
            res
                .status(500)
                .json(err);
        });
});

// ERROR HANDLING ----------------------------------------------------------------------------------------------------
// Handles 404. In Express, 404 responses are not the result of an error,
// so the error-handler middleware will not capture them.
// To handle a 404 response, add a middleware function at the very bottom of the stack
// (below all other path handlers)
app.use(function (req, res) {
    res.status(404).sendFile(path.join(MSG_FOLDER + "/404.html"));
});

// Error handler: server error
app.use(function (err, req, res, next) {
    res.status(501).sendFile(path.join(MSG_FOLDER + '/501.html'));
});


// SERVER / PORT SETUP ------------------------------------------------------------------------------------------------
// Server starts and listens on NODE_PORT
app.listen(NODE_PORT, function () {
    console.log("Server running at http://localhost:" + NODE_PORT);
});
