// TODO: 6.1 Define product table model
// Model for product table
// References:
// http://docs.sequelizejs.com/en/latest/docs/getting-started/#your-first-model
// http://docs.sequelizejs.com/en/latest/docs/models-definition/
//Create a model for product table
module.exports = function(sequelize, Sequelize) {
    var Product=  sequelize.define('product', {
        id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        upc12: {
            type: Sequelize.BIGINT(12),
            allowNull: false
        },
        brand: {
            type: Sequelize.STRING(255),
            allowNull: false
        },
        name: {
            type: Sequelize.STRING(255),
            allowNull: false
        },
    }, {
        tableName: 'grocery_list',
        timestamps: false
    });
    return Product;
};