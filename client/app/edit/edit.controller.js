// TODO: Search(specific), Update, Delete
// TODO: 3. Build controller for edit html. Should support functionalities listed in edit.html
(function () {
    'use strict';
    angular
        .module("GMS")
        .controller("EditCtrl", EditCtrl);

    EditCtrl.$inject = ["$filter", "ProductService", "$stateParams"];

    function EditCtrl($filter, ProductService, $stateParams) {

        // Declares the var vm (for ViewModel) and assigns it the object this. Any function or variable that you attach
        // to vm will be exposed to callers of EditCtrl, e.g., edit.html
        var vm = this;

        // Exposed data models -----------------------------------------------------------------------------------------
        // Creates a department object. We expose the department object by attaching it to the vm. This allows us to
        // apply two-way data-binding to this object by using ng-model in our view (i.e., index.html)
        vm.id = "";
        vm.result = {};

        // Exposed functions ------------------------------------------------------------------------------------------
        // Exposed functions can be called from the view.
        vm.initDetails = initDetails;
        vm.search = search;
        // vm.toggleEditor = toggleEditor;
        vm.updateProductName = updateProductName;


        // Initializations --------------------------------------------------------------------------------------------
        // Functions that are run when view/html is loaded
        initDetails();

        //if parameter passing
        if ($stateParams.id) {
            vm.result.id = $stateParams.id;
            console.log("result.id =" +  vm.result.id)  
            vm.search();    
        }

        



        // Function declaration and definition -------------------------------------------------------------------------


        // Initializes product details shown in view
        function initDetails() {
            console.log("-- show.controller.js > initDetails()");
            vm.result.id = "";
            vm.result.upc12 = "";
            vm.result.brand = "";
            vm.result.name = "";
            vm.showDetails = false;
            vm.isEditorOn = false;
        }



//search
function search() {
           // vm.showProduct = false;
            ProductService
                // we pass contents of vm.searchString to service so that we can search the DB for this string
                .retrieveProductByID(vm.result.id)
                .then(function (results) {
                    // The result returned by the DB contains a data object, which in turn contains the records read
                    // from the database
                    console.log(results.data);
                    vm.result = results.data;
                })
                .catch(function (err) {
                    // We console.log the error. For a more graceful way of handling the error, see
                    // register.controller.js
                    console.log("error " + err);
                });
        }




        // Saves edited upc12 name
        function updateUPC12Name() {
            console.log("-- show.controller.js > save()");
            ProductService
                .updateUPC12(vm.id, vm.result.upc12)
                .then(function (result) {
                    console.log("-- show.controller.js > save() > results: \n" + JSON.stringify(result.data));
                })
                .catch(function (err) {
                    console.log("--  show.controller.js > save() > error: \n" + JSON.stringify(err));
                });
            vm.toggleEditor();
        }

        // Saves edited brand name
        function updateBrandName() {
            console.log("-- show.controller.js > save()");
            ProductService
                .updateBrand(vm.id, vm.result.brand)
                .then(function (result) {
                    console.log("-- show.controller.js > save() > results: \n" + JSON.stringify(result.data));
                })
                .catch(function (err) {
                    console.log("--  show.controller.js > save() > error: \n" + JSON.stringify(err));
                });
            vm.toggleEditor();
        }

        // Saves edited product name
        function updateProductName() {
            console.log("-- show.controller.js > save()");
            ProductService
                .updateProduct(vm.id, vm.result.name)
                .then(function (result) {
                    console.log("-- show.controller.js > save() > results: \n" + JSON.stringify(result.data));
                })
                .catch(function (err) {
                    console.log("--  show.controller.js > save() > error: \n" + JSON.stringify(err));
                });
            vm.toggleEditor();
        }
    }
})();