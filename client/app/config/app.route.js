(function () {
    angular
        .module("GMS")
        .config(uiRouteConfig);

    uiRouteConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function uiRouteConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state("searchDB", {
                url: "/search",
                templateUrl: "app/search/searchDB.html",
                            })
            .state("editWithParams", { //parameter passing
                url: "/edit/:id",
                templateUrl: "app/edit/edit.html",
                controller: "EditCtrl",
                controllerAs: "ctrl"
            });

        $urlRouterProvider.otherwise("/search");

    }

})();

