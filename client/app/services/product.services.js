// Always use an IIFE, i.e., (function() {})();
(function () {
    // Attaches DeptService service to the DMS module
    angular
        .module("GMS")
        .service("ProductService", ProductService);

    // Dependency injection. Here we inject $http because we need this built-in service to communicate with the server
    // There are different ways to inject dependencies; $inject is minification safe
    ProductService.$inject = ['$http'];

    // DeptService function declaration
    // Accepts the injected dependency as a parameter. We name it $http for consistency, but you may assign any name
    function ProductService($http) {

        // Declares the var service and assigns it the object this (in this case, the DeptService). Any function or
        // variable that you attach to service will be exposed to callers of DeptService, e.g., search.controller.js
        // and register.controller.js
        var service = this;

        // EXPOSED DATA MODELS -----------------------------------------------------------------------------------------
        // EXPOSED FUNCTIONS -------------------------------------------------------------------------------------------
        service.insertProduct = insertProduct;
        service.retrieveProductDB = retrieveProductDB;
        service.retrieveProductByID = retrieveProductByID;
        service.retrieveProductByBrand = retrieveProductByBrand;
        service.retrieveProductByName = retrieveProductByName;
        service.updateProduct = updateProduct;


        // FUNCTION DECLARATION AND DEFINITION -------------------------------------------------------------------------

        // insertDept uses HTTP POST to send department information to the server's /departments route
        // Parameters: department information; Returns: Promise object
        function insertProduct(product) {
            // This line returns the $http to the calling function
            // This configuration specifies that $http must send the department data received from the calling function
            // to the /departments route using the HTTP POST method. $http returns a promise object. In this instance
            // the promise object is returned to the calling function\

            return $http({
                method: 'POST'
                , url: 'api/product'
                , data: {prdt: product}
            });
        }


        // retrieveDeptDB retrieves department information from the server via HTTP GET. Passes information via the query
        // string (params) Parameters: searchString. Returns: Promise object
        function retrieveProductDB(searchString) {
            return $http({
                method: 'GET'
                , url: 'api/product'
                , params: {
                    'searchString': searchString
                }
            });
        }

        // retrieveDeptByID retrieves department information from the server via HTTP GET. Passes information as a
        // route parameter
        function retrieveProductByID(id) {
            return $http({
                method: 'GET'
                , url: "api/product/" + id
            });
        }

        // retrieveDeptManager retrieves department information from the server via HTTP GET.
        // Parameters: searchString. Returns: Promise object
        function retrieveProductByBrand(searchString) {
            return $http({
                method: 'GET'
                , url: 'api/product' + brand
                , params: {
                    'searchString': searchString
                }
            });
        }

        // retrieveDeptManager retrieves department information from the server via HTTP GET.
        // Parameters: searchString. Returns: Promise object
        function retrieveProductByName(searchString) {
            return $http({
                method: 'GET'
                , url: 'api/product' + name
                , params: {
                    'searchString': searchString
                }
            });
        }

        // updateDept uses HTTP PUT to update department name saved in DB; passes information as route parameters and via
        // HTTP HEADER BODY IMPORTANT! Route parameters are not the same as query strings!
        function updateProduct(id, upc12, brand, name) {
            return $http({
                method: 'PUT'
                , url: 'api/product/' + id
                , data: {
                    id: id,
                    upc12: upc12,
                    brand: brand,
                    name: name
                }
            });
        }
    }
})();